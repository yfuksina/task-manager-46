package ru.tsc.fuksina.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

}
