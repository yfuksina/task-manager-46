package ru.tsc.fuksina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        super(token);
        this.id = id;
        this.status = status;
    }

}
