package ru.tsc.fuksina.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserShowProfileRequest extends AbstractUserRequest {

    public UserShowProfileRequest(@Nullable final String token) {
        super(token);
    }

}
