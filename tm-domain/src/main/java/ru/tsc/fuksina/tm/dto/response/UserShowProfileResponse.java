package ru.tsc.fuksina.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserShowProfileResponse extends AbstractUserResponse {

    public UserShowProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
