package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void remove(@NotNull M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
