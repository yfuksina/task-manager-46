package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.model.IUserRepository;
import ru.tsc.fuksina.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT m  FROM User m", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }


    @Override
    public User findOneByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT m FROM User m WHERE m.login = :login", User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT m FROM User m WHERE m.email = :email", User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(User.class, id));
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.createQuery("DELETE FROM User m WHERE m.login = :login", User.class)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void clear() {
        @NotNull final List<User> users = findAll();
        users.forEach(this::remove);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM User m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM User", Long.class)
                .getSingleResult();
    }

}
