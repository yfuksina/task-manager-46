package ru.tsc.fuksina.tm.api.repository.dto;

import ru.tsc.fuksina.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {
}
