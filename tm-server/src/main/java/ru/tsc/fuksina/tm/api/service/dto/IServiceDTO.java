package ru.tsc.fuksina.tm.api.service.dto;

import ru.tsc.fuksina.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

}
