package ru.tsc.fuksina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
