package ru.tsc.fuksina.tm.api.service.model;

import ru.tsc.fuksina.tm.api.repository.model.IRepository;
import ru.tsc.fuksina.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
