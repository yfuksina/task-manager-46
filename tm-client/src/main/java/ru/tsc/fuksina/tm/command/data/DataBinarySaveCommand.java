package ru.tsc.fuksina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.request.DataBinarySaveRequest;
import ru.tsc.fuksina.tm.enumerated.Role;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data in binary file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE DATA IN BINARY FILE]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

}
