package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete project by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED));
    }

}
