package ru.tsc.fuksina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.request.UserShowProfileRequest;
import ru.tsc.fuksina.tm.dto.response.UserShowProfileResponse;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.entity.UserNotFoundException;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        @NotNull UserShowProfileResponse response = serviceLocator.getAuthEndpoint().profile(new UserShowProfileRequest(getToken()));
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}
