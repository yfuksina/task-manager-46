package ru.tsc.fuksina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.request.DataXmlJaxbLoadRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.enumerated.Role;

public final class DataXmlJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD DATA FROM XML FILE]");
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlJaxbLoadRequest(getToken()));
        serviceLocator.getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
